import { Component, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
//import {Form} from './form';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  // template: `Name : <input type="text" [(ngModel)]="name">`
})
export class myFormComponent {
  constructor(private service: ProductService) { console.log(' my Form constructor called '+this.service.productData)}


  resetForm(form?: NgForm) {
    if(form !=null)
      form.resetForm();
    this.service.productData = {
      name: '',
      category: '',
      color: '',
      availableQuantity: 0,
      unitPrice: 0,
      cratedDate:new Date()
    }
  }

   onSubmit = (form: NgForm) =>{
    console.log('on submit = ' + form.value.cratedDate)
    this.service.postProductDetail(form.value)
      .subscribe(
        res => { this.resetForm(form)},
        err => { console.log(err)}
      )
  }
}
