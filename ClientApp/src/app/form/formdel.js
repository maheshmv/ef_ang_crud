"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Formddd = void 0;
var Formddd = /** @class */ (function () {
    function Formddd(
    //public id: number,
    name, color, category, unitPrice, availableQuantity, createdDate) {
        this.name = name;
        this.color = color;
        this.category = category;
        this.unitPrice = unitPrice;
        this.availableQuantity = availableQuantity;
        this.createdDate = createdDate;
    }
    return Formddd;
}());
exports.Formddd = Formddd;
//# sourceMappingURL=form.js.map