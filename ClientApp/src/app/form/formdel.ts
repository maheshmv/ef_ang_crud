export class Formddd {

  constructor(
    //public id: number,
    public name: string,
    public color: string,
    public category: string,
    public unitPrice: number,
    public availableQuantity: number,
    public createdDate: Date,

  ) { }

}
