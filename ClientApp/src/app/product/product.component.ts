import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html'
})
export class ProductComponent {
  public product: angProducts[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<angProducts[]>(baseUrl + 'product').subscribe(result => {
      this.product = result;
    }, error => console.error(error));
  }
}

interface angProducts {
  productid: number;
  name: string;
  color: string;
  category: string;
  availablequantity: number;
  unitprice: number;
  crateddate: Date;
}
