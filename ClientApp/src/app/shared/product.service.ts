import { Injectable, Inject } from '@angular/core';
import { productDetail } from './productDetail';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ProductService {
  productData: productDetail = new productDetail();
  url: string
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.url = baseUrl;
    //console.log('in service , baseurl= ' + baseUrl)
  }

  postProductDetail(data: productDetail) {
    console.log('in service , data= ' + data.color);
    return this.http.post(this.url +'products',data);
  }
}
