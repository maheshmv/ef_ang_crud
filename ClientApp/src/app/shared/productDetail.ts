export class productDetail {
  name: string;
  category: string;
  color: string
  unitPrice: number;
  availableQuantity: number;
  cratedDate: Date
}
