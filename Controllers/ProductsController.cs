﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using efAng.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace efAng.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        InventoryContext _context;

        public ProductsController(InventoryContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Products> Get()
        {

            return _context.Products.ToList();

        }

        [HttpPost]
        public async Task Post(Products p)
        {
            _context.Products.Add(p);
            await _context.SaveChangesAsync();
        }
    }
}
