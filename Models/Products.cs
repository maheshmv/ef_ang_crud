﻿using System;
using System.Collections.Generic;

namespace efAng.Models
{
    public partial class Products
    {
        public long ProductId { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Color { get; set; }
        public int UnitPrice { get; set; }
        public int AvailableQuantity { get; set; }
        public DateTime CratedDate { get; set; }
    }
}
