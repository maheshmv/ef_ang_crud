using System;

namespace efAng
{
    public class _Product
    {
        public long Id { get; set; }

        public string Name { get; set; }
        public string Category { get; set; }
        public string Color { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal AvailableQuantity { get; set; }
        public DateTime CratedDate { get; set; }

    }
}
